package id.artivisi.training.microservices.wallet.controller;

import id.artivisi.training.microservices.wallet.dao.WalletTransactionDao;
import id.artivisi.training.microservices.wallet.entity.Wallet;
import id.artivisi.training.microservices.wallet.entity.WalletTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class WalletTransactionController {

    @Autowired private WalletTransactionDao walletTransactionDao;

    @GetMapping("/wallet/{id}/transaction")
    public Iterable<WalletTransaction> findByWallet(@PathVariable("id") Wallet wallet){
        return walletTransactionDao.findByWalletOrderByTransactionTime(wallet);
    }

    @GetMapping("/hostinfo")
    public Map<String, Object> hostInfo(HttpServletRequest request) throws UnknownHostException {
        Map<String, Object> info = new HashMap<>();
        info.put("hostname", InetAddress.getLocalHost().getHostName());
        info.put("address", request.getLocalAddr());
        info.put("port", request.getLocalPort());
        return info;
    }

}
